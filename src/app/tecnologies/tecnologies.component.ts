import { Component, OnInit } from '@angular/core';
import {TechnologiesService} from '../services/technologies.service';
import { Router } from '@angular/router';
import {Technology} from '../model/technology';

@Component({
  selector: 'app-tecnologies',
  templateUrl: './tecnologies.component.html',
  styleUrls: ['./tecnologies.component.css'],
  providers: [TechnologiesService]
})
export class TecnologiesComponent implements OnInit {

  constructor(private router: Router, private technologiesService:TechnologiesService) { }

  technologies:Array<Technology> = new Array<Technology>();
  technologiesResponse: [];
  public page:number;

  ngOnInit() 
  {
    this.technologiesService.GetTechnologies().then(response => {
      this.technologiesResponse =  response.technologies
      this.technologies = this.technologiesResponse
      console.log(response.units)
    }).catch(response => {
      console.log(response);
    })
  }
}
