import { Component, OnInit } from '@angular/core';
import {UnitsService} from '../services/units.service';
import {Unit} from '../model/unit';
import { Router } from '@angular/router';

@Component({
  selector: 'app-units',
  templateUrl: './units.component.html',
  styleUrls: ['./units.component.css'],
  providers: [UnitsService]
})
export class UnitsComponent implements OnInit {

  constructor(private router: Router, private unitsService:UnitsService) { }

  units:Array<Unit> = new Array<Unit>();
  unitsResponse: [];
  public page:number;

  ngOnInit() 
  {
    this.unitsService.GetUnits().then(response => {
      this.unitsResponse =  response.units
      this.units = this.unitsResponse
      console.log(response.units)
    }).catch(response => {
      console.log(response);
    })
  }
}
