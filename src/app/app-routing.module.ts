import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { CivilizationsComponent } from './civilizations/civilizations.component';
import { StructuresComponent } from './structures/structures.component';
import { TecnologiesComponent } from './tecnologies/tecnologies.component';
import { UnitsComponent } from './units/units.component';

const routes: Routes = [
  {path:"", component: MainComponent},
  {path:"civilizations", component: CivilizationsComponent},
  {path:"units", component: UnitsComponent},
  {path:"structures", component:  StructuresComponent},
  {path:"tecnologies", component: TecnologiesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
