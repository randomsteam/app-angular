import { Injectable } from '@angular/core';
import {HttpHeaders,HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class StructuresService {

  private API:string = "https://aoe2-app.herokuapp.com/api/v1/"
  
  constructor(private http:HttpClient) { }

  public GetStructures():Promise<any>
  {
    let headers = new HttpHeaders;

    headers.append("content-type","application/x-www-form-uncode; charset=utf-8");

    return this.http.get(`${this.API}/structures`,{headers:headers}).toPromise();
  }
}
