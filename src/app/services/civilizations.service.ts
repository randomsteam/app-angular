import { Injectable } from '@angular/core';
import {HttpHeaders,HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class CivilizationsService  {

  private API:string = "https://aoe2-app.herokuapp.com/api/v1/"

  constructor(private http:HttpClient) { }

  public GetCivilization():Promise<any>
  {
    let headers = new HttpHeaders;

    headers.append("content-type","application/x-www-form-uncode; charset=utf-8");

    return this.http.get(`${this.API}/civilizations`,{headers:headers}).toPromise();
  }
}
