import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {CivilizationsService} from '../services/civilizations.service';
import {Civilization} from '../model/civilization';

@Component({
  selector: 'app-civilizations',
  templateUrl: './civilizations.component.html',
  styleUrls: ['./civilizations.component.css'],
  providers: [CivilizationsService]
})

export class CivilizationsComponent implements OnInit 
{

  constructor(private router: Router, private civilizationsService:CivilizationsService) { }

  civilizations:Array<Civilization> = new Array<Civilization>();
  civilizationsResponse: [];
  public page:number;

  ngOnInit() 
  {
    this.civilizationsService.GetCivilization().then(response => {
      this.civilizationsResponse =  response.civilizations
      this.civilizations = this.civilizationsResponse
    }).catch(response => {
      console.log(response);
    })
  }
}